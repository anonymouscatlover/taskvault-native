![TaskVault Logo](./Client/Assets/taskvault-logo.png)

# TaskVault

TaskVault is a [Free and Open Source](https://www.gnu.org/philosophy/free-sw.html) 
privacy-friendly [Getting-Things-Done](https://gettingthingsdone.com/what-is-gtd/) 
compatible personal project management and productivity application for native
platforms.

- designed to enable enhanced productivity without sacrificing data privacy
- manage life's complexity in confidence, without worrying whether unintended parties may gain access 
to sensitive information.

**Supported Platforms:** Windows, Linux

**Planned Future Support:** Android, OSX, iOS

[**Download Here**](https://gitlab.com/taskvault/taskvault-native/-/releases)

## Report A Bug

[View and Submit Bug Reports Here](https://taskvault.canny.io/bug-reports)

## Features Overview

**Note:** TaskVault is in early development and as such, features and UI/UX are 
expected to change and improve

![TaskVault Screen Shot 01](Client/Assets/Screenshots/taskvault-screenshot-01.png)

- Create a local (offline) task-vault to get started

![TaskVault Screen Shot 04](Client/Assets/Screenshots/taskvault-screenshot-04.png)

- Use Inbox to capture active tasks

![TaskVault Screen Shot 02](Client/Assets/Screenshots/taskvault-screenshot-02.png)

- Organize tasks into actionable goals via the project-hierarchy:
Project Groups -> Projects -> Tasks

![TaskVault Screen Shot 03](Client/Assets/Screenshots/taskvault-screenshot-03.png)

![TaskVault Screen Shot 05](Client/Assets/Screenshots/taskvault-screenshot-05.png)

![TaskVault Screen Shot 06](Client/Assets/Screenshots/taskvault-screenshot-06.png)

## Planned Features

[**View Roadmap**](https://taskvault.canny.io/)

[**Request a Feature**](https://taskvault.canny.io/feature-requests)

## What is Getting Things Done?

Getting Things Done is a personal productivity system developed by David Allen and detailed in a
published and well regarded book of the same name.

**Note**: TaskVault and its contributors are NOT affiliated with David Allen Company, 
Getting Things Done, or any related business entities or organizations. 

Instead, TaskVault is an independent tool developed to be utilized
as part of a personal Getting-Things-Done-system implementation. Of course, there is no requirement to practice
GTD when using TaskVault; users are free to employ TaskVault in whichever ways work best for their
use case and circumstances.

Please see the following resources for more information on Getting Things Done:

[Getting Things Done official website](https://gettingthingsdone.com/)

[Getting Things Done (ebook)](https://www.ebooks.com/en-us/book/1710332/getting-things-done/david-allen/)

## Please Support the Project

<noscript><a href="https://liberapay.com/TaskVault/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

In addition to being freedom-software, the base application will remain free-of-cost.

If you would like to sponsor this project, please consider [scheduling a recurring
donation](https://liberapay.com/TaskVault/)

Your support is highly appreciated!

## About the Developer

Developed by Tyler Hasty [(website)](https://tylerhasty.com)

Branding Graphics by Erik Hasty

## Development Info

TaskVault native desktop and mobile applications implemented in C# 
with [AvaloniaUI](http://avaloniaui.net/) crossplatform framework

### Build Instructions

TODO

(for now, reference .gitlab-ci.yml and docker-compose.yml)

## Feedback and Questions

[Submit General Feedback or Questions About TaskVault](https://forms.clickup.com/14110750/f/dem0y-1304/LAGNA7RM42DOL1UDZ8)

## Licensing and Copying Info

![GPLv3Logo](./Client/Assets/gplv3-with-text-136x68.png)

Please see LICENSE-NOTICE.txt and COPYING.txt for TaskVault licensing information.