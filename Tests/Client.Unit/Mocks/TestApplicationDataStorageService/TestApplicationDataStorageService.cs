// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Collections.ObjectModel;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client;

public class TestApplicationDataStorageService : IApplicationDataStorageService
{
    public bool LoadLocalVaultsWasCalled { get; protected set; }

    public void SaveContent<TModel>(TModel contentModel)
    {
    }

    public LocalVaultModel LoadVaultByName(string vaultName)
    {
        return null;
    }

    public virtual IEnumerable<LocalVaultModel> LoadLocalVaults()
    {
        LoadLocalVaultsWasCalled = true;
        var localVaultModels = new List<LocalVaultModel>();
        var index = 0;
        while (index < 30)
        {
            localVaultModels.Add(new LocalVaultModel { Name = $"TestVault{index}" });
            index += 1;
        }

        return localVaultModels;
    }

    public virtual IEnumerable<ProjectGroupModel> LoadProjectGroupsForVault(LocalVaultModel localVaultModel)
    {
        var projectGroupModels = new List<ProjectGroupModel>();
        var index = 0;
        while (index < 30)
        {
            projectGroupModels.Add(new ProjectGroupModel
                { Name = $"Project Group {index}", LocalVault = localVaultModel });
            index += 1;
        }

        return projectGroupModels;
    }

    public virtual IEnumerable<ProjectModel> LoadProjectsInProjectGroup(ProjectGroupModel projectGroupModel)
    {
        var projectModels = new List<ProjectModel>();
        var index = 0;
        while (index < 30)
        {
            projectModels.Add(new ProjectModel { Name = $"Project {index}", ProjectGroup = projectGroupModel });
            index += 1;
        }

        return projectModels;
    }

    public virtual IEnumerable<TaskContextModel> LoadTaskContextsForVault(LocalVaultModel localVaultModel)
    {
        var taskContexts = new List<TaskContextModel>();
        var index = 0;
        while (index < 10)
        {
            taskContexts.Add(new TaskContextModel { Name = $"Task Context {index}", LocalVault = localVaultModel });
            index += 1;
        }

        return taskContexts;
    }

    public virtual IEnumerable<TaskModel> LoadTasksInProject(ProjectModel projectModel)
    {
        var tasks = new List<TaskModel>();
        var index = 0;
        while (index < 30)
        {
            tasks.Add(new TaskModel { Name = $"Task {index}", Project = projectModel });
            index += 1;
        }

        return tasks;
    }

    public void DeleteContent<TModel>(TModel contentModel)
    {
    }

    public virtual IEnumerable<TaskModel> LoadTasksInInbox(LocalVaultModel localVaultModel)
    {
        var tasks = new List<TaskModel>();
        var index = 0;
        while (index < 30)
        {
            tasks.Add(new TaskModel { Name = $"Task {index}" });
            index += 1;
        }

        return tasks;
    }

    public void DeleteProjectGroup(ProjectGroupModel projectGroupModel)
    {
    }

    public void DeleteProject(ProjectModel projectModel)
    {
    }

    public void DeleteTask(TaskModel taskModel)
    {
    }

    public IEnumerable<ProjectModel> LoadProjectsInLocalVault(LocalVaultModel localVaultModel)
    {
        var projectModels = new List<ProjectModel>();
        var index = 0;
        var projectGroup = new ProjectGroupModel { Name = "Test Project Group", LocalVault = localVaultModel };
        while (index < 30)
        {
            projectModels.Add(new ProjectModel
                { Name = $"Project {index}", ProjectId = index + 1, ProjectGroup = projectGroup });
            index += 1;
        }

        return projectModels;
    }

    public LocalVaultModel LoadLocalVaultForProjectGroup(ProjectGroupModel projectGroupModel)
    {
        return new LocalVaultModel
        {
            Name = "Test Local Vault", ProjectGroups = new ObservableCollection<ProjectGroupModel> { projectGroupModel }
        };
    }

    public LocalVaultModel LoadLocalVaultForProject(ProjectModel projectModel)
    {
        return new LocalVaultModel { Name = "Test Local Vault" };
    }
}