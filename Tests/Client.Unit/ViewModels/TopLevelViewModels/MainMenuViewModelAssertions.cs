// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class MainMenuViewModelAssertions
{
    public MainMenuViewModel CreateMainMenuViewModel()
    {
        return new(new TestScreen(), new TestRoutableViewModelFactory(),
            new TestEmbeddedViewModelFactory());
    }

    [Test]
    public void SpecifiesUrl()
    {
        var viewModel = CreateMainMenuViewModel();
        viewModel.UrlPathSegment.Should().Be("main-menu");
    }

    [Test]
    public void PresentsViewHeader()
    {
        var viewModel = CreateMainMenuViewModel();

        viewModel.Header.Should().StartWith("Main Menu");
    }

    [Test]
    public void HasHeaderMenu()
    {
        var viewModel = CreateMainMenuViewModel();

        viewModel.HeaderMenu.Should().BeNull();
    }

    [Test]
    public void HasEmbeddedListBox()
    {
        var viewModel = CreateMainMenuViewModel();

        viewModel.ActionListsPaginatedList.Should().BeNull();
    }

    [Test]
    public void SpecifiesProjectGroupsListBoxControlName()
    {
        var viewModel = CreateMainMenuViewModel();

        viewModel.ActionListsListBoxControlName.Should().Be("ActionListsListBox");
    }

    [Test]
    public void SpecifiesNoActionListsMessage()
    {
        var viewModel = CreateMainMenuViewModel();

        viewModel.NoActionListsMessage.Should().Be("No Action Lists created for this vault");
    }

    [Test]
    public void StoresActiveLocalVault()
    {
        var viewModel = CreateMainMenuViewModel();

        viewModel.ActiveLocalVault.Should().BeOfType<LocalVaultModel>();
    }
}