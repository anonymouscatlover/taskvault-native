// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class ProjectGroupsViewModelAssertions
{
    private ProjectGroupsViewModel CreateProjectGroupsViewModel()
    {
        return new(new TestScreen(), new TestEmbeddedViewModelFactory(),
            new TestRoutableViewModelFactory(), new TestApplicationDataStorageService());
    }

    [Test]
    public void HasProjectGroupsHeader()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.ProjectGroupsHeader.Should().Be("Project Groups");
    }

    [Test]
    public void HasEmbeddedDetailsScreen()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.DetailsScreen.Should().BeNull();
    }

    [Test]
    public void SpecifiesNoContentMessageForEmbeddedDetailsScreen()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.DetailsScreenNoContentMessage.Should().Be("Select Project Group to view details");
    }

    [Test]
    public void HasHeaderMenu()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.HeaderMenu.Should().BeNull();
    }

    [Test]
    public void SpecifiesMaximumOf10ProjectGroupsPerPage()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.ProjectGroupsPerPage.Should().Be(10);
    }

    [Test]
    public void SpecifiesMessageForEmptyProjectGroupsList()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.NoProjectGroupsMessage.Should()
            .Be("No project groups in this vault. Create a project group to get started");
    }

    [Test]
    public void HasEmbeddedListBox()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.ProjectGroupsPaginatedList.Should().BeNull();
    }

    [Test]
    public void SpecifiesNoProjectGroupsMessageControlName()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.NoProjectGroupsMessageControlName.Should().Be("NoProjectGroupsMessage");
    }

    [Test]
    public void SpecifiesProjectGroupsListBoxControlName()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.ProjectGroupsListBoxControlName.Should().Be("ProjectGroupsListBox");
    }

    [Test]
    public void StoresActiveLocalVault()
    {
        var viewModel = CreateProjectGroupsViewModel();

        viewModel.ActiveLocalVault.Should().BeOfType<LocalVaultModel>();
    }
}