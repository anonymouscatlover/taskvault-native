// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class AboutViewModelAssertions
{
    private AboutViewModel CreateAboutViewModel() => new(new TestScreen());

    [Test]
    public void SpecifiesLogoSource()
    {
        var viewModel = CreateAboutViewModel();

        viewModel.AppLogoSource.Should().Be(Path.ChangeExtension(Path.Join("/Assets", "taskvault-logo"), ".png"));
    }
    
    [Test]
    public void SpecifiesAppTitle()
    {
        var viewModel = CreateAboutViewModel();

        viewModel.AppTitle.Should().Be("TaskVault");
    }
    
    [Test]
    public void SpecifiesContributorCredits()
    {
        var viewModel = CreateAboutViewModel();

        viewModel.DeveloperCredit.Should().Be("Developed by Tyler Hasty: ");
        viewModel.DeveloperWebsiteUrl.Should().Be("https://tylerhasty.com/");
        viewModel.BrandingGraphicsCredit.Should().Be("Branding Graphics by Erik Hasty");
    }

    [Test]
    public void SpecifiesSourceCodeReference()
    {
        var viewModel = CreateAboutViewModel();

        viewModel.SourceCodeReferenceLabel.Should().Be("Source Code Available Here: ");
        viewModel.SourceCodeReferenceUrl.Should().Be("https://gitlab.com/taskvault/taskvault-native");
    }

    [Test]
    public void SpecifiesProjectSupportPlatformReference()
    {
        var viewModel = CreateAboutViewModel();

        viewModel.ProjectSupportPlatformReferenceLabel.Should().Be("Support the project: ");
        viewModel.ProjectSupportPlatformReferenceUrl.Should().Be("https://liberapay.com/TaskVault/");
    }

    [Test]
    public void SpecifiesCopyright()
    {
        var viewModel = CreateAboutViewModel();

        viewModel.Copyright.Should().Be("Copyright 2022 Tyler Hasty");
    }

    [Test]
    public void SpecifiesLicenseNotice()
    {
        var viewModel = CreateAboutViewModel();
        
        viewModel.LicenseNotice.Should()
            .Be("TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public " +
                "License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version." +
                "\nTaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of " +
                "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details." +
                "\nYou should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.");
    }

    [Test]
    public void SpecifiesAppVersion()
    {
        var viewModel = CreateAboutViewModel();

        viewModel.Version.Should().MatchRegex("v[0-9]+.[0-9]+.[0-9]+");
    }
}
