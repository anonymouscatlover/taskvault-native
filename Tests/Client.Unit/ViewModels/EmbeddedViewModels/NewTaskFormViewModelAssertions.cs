// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class NewTaskFormViewModelAssertions
{
    private NewTaskFormViewModel CreateNewTaskFormViewModel()
    {
        return new(new TestScreen(), new TestApplicationDataStorageService());
    }

    [Test]
    public void HasHeader()
    {
        var viewModel = CreateNewTaskFormViewModel();

        viewModel.Header.Should().Be("New Task");
    }

    [Test]
    public void RaisesValidationErrorIfNameIsBlank()
    {
        var viewModel = CreateNewTaskFormViewModel();
        var invalidInput = string.Empty;

        viewModel.TaskName = invalidInput;
        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should().Contain("Task name cannot be blank");
    }

    [Test]
    public void RaisesValidationErrorIfNameExceeds256Characters()
    {
        var viewModel = CreateNewTaskFormViewModel();
        var invalidInput = "3Z4dXM7MA8FqZwwQ2caFgGqenqTrX9fbAAnN8UBJPTTXY2eHTdRAER5qQaSPzKhu7ixCWZF7LL" +
                           "MNLP3HSnCPWJf4BVkJpW2yMRvASDbuYmFGbhV98TdqxN5CB6pzqhXngTHDvC3vBAyaJUGvM2jRTqmYSM" +
                           "JcziLN9Rh4wdkJTvBSmQ9ntYnAUmJea78cyQ5tMpLr3eJchNz4ui6AGSUwBv29LWbRYyb854QbhNNAvN" +
                           "zUvdLmnHmnZmrKzhRwQc257";

        viewModel.TaskName = invalidInput;
        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should().Contain("Task name cannot exceed 256 characters");
    }

    [Test]
    public void RaisesValidationErrorIfNameUsesInvalidCharacters()
    {
        var viewModel = CreateNewTaskFormViewModel();
        var invalidInput = "#invalid name";

        viewModel.TaskName = invalidInput;
        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should()
            .Contain("Task name may include the following characters:\n A-Z a-z 0-9 . - _ WHITESPACE");
    }

    [Test]
    public void RaisesValidationErrorIfDescriptionExceeds5000Characters()
    {
        var viewModel = CreateNewTaskFormViewModel();
        var invalidInput = "";
        var count = 0;
        while (count <= 5000)
        {
            invalidInput += "0";
            count++;
        }

        viewModel.TaskDescription = invalidInput;
        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should().Contain("Task description may not exceed 5000 characters");
    }

    [Test]
    public void HasTaskContextsCollection()
    {
        var viewModel = CreateNewTaskFormViewModel();

        viewModel.TaskContexts.Should().BeNullOrEmpty();
    }

    [Test]
    public void HasSelectedTaskContexts()
    {
        var viewModel = CreateNewTaskFormViewModel();

        viewModel.SelectedTaskContexts.Should().BeEmpty();
    }

    [Test]
    public void LoadsExistingTaskContexts()
    {
        // note: contexts configured in TestLocalVaultSessionService.LoadTaskContexts()
        var viewModel = CreateNewTaskFormViewModel();

        viewModel.LoadedTaskContexts.Should().NotBeNull();
    }

    [Test]
    public void SpecifiesNoTaskContextsMessage()
    {
        var viewModel = CreateNewTaskFormViewModel();

        viewModel.NoTaskContextsMessage.Should().Be("No task contexts exist for this vault. " +
                                                    "To assign a task context to this task, first create a task context");
    }

    [Test]
    public void StoresActiveLocalVault()
    {
        var viewModel = CreateNewTaskFormViewModel();

        viewModel.ActiveLocalVault.Should().BeOfType<LocalVaultModel>();
    }

    [Test]
    public void StoresActiveProject()
    {
        var viewModel = CreateNewTaskFormViewModel();

        viewModel.ActiveProject.Should().BeOfType<ProjectModel>();
    }
}