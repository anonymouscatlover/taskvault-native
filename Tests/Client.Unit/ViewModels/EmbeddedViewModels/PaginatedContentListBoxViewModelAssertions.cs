// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Exceptions;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class PaginatedContentListBoxViewModelAssertions
{
    public PaginatedContentListBoxViewModel<TestModel> CreatePaginatedContentListBox()
    {
        return new(new TestScreen());
    }

    [Test]
    public void HasNameWhichDefaultsToContentListBox()
    {
        var viewModel = CreatePaginatedContentListBox();

        viewModel.Name.Should().Be("ContentListBox");
    }

    [Test]
    public void HasCurrentPageOfContent()
    {
        var viewModel = CreatePaginatedContentListBox();

        viewModel.CurrentPageContent.Should().BeAssignableTo<IEnumerable<TestModel>>();
    }

    [Test]
    public void TracksWhetherContentCollectionIsEmpty()
    {
        var viewModel = CreatePaginatedContentListBox();

        viewModel.IsEmpty.Should().BeTrue();

        viewModel.FullContent = new List<TestModel> { new() { Name = "Test Model" } };

        viewModel.IsEmpty.Should().BeFalse();
    }

    [Test]
    public void HasNoContentMessage()
    {
        var viewModel = CreatePaginatedContentListBox();

        viewModel.NoContentMessage.Should().Be("No content");
    }

    [Test]
    public void CanShiftToNextPageIfOneExists()
    {
        var viewModel = CreatePaginatedContentListBox();
        var count = 0;
        var testModelList = new List<TestModel>();
        while (count < 30)
        {
            testModelList.Add(new TestModel { Name = $"TestModel{count + 1}" });
            count++;
        }

        viewModel.FullContent = testModelList;

        var initialPageContent = new List<TestModel>();
        initialPageContent.AddRange(viewModel.CurrentPageContent);
        viewModel.ShiftToNextPage.Execute().Subscribe();

        viewModel.CurrentPageContent.Intersect(initialPageContent).Should().BeEmpty();
    }

    [Test]
    public void CanShiftToPreviousPageIfOneExists()
    {
        var viewModel = CreatePaginatedContentListBox();
        var count = 0;
        var testModelList = new List<TestModel>();
        while (count < 30)
        {
            testModelList.Add(new TestModel { Name = $"TestModel{count + 1}" });
            count++;
        }

        viewModel.FullContent = testModelList;
        viewModel.ShiftToNextPage.Execute().Subscribe();

        var initialPageContent = viewModel.CurrentPageContent.ToList();
        viewModel.ShiftToPreviousPage.Execute().Subscribe();

        viewModel.CurrentPageContent.Intersect(initialPageContent).Should().BeEmpty();
    }

    [Test]
    public void SpecifiesMaxItemsPerPageWhichDefaultsTo10Items()
    {
        var viewModel = CreatePaginatedContentListBox();
        var count = 0;
        var testModelList = new List<TestModel>();
        while (count < 30)
        {
            testModelList.Add(new TestModel { Name = $"TestModel{count + 1}" });
            count++;
        }

        viewModel.FullContent = testModelList;

        viewModel.CurrentPageContent.Count().Should().Be(10);
    }

    [Test]
    public void CanSpecifySingleItemAsSelected()
    {
        var viewModel = CreatePaginatedContentListBox();

        viewModel.SelectedItem.Should().BeNull();
    }

    [Test]
    public void WillNotShiftToNextPageFromLastPage()
    {
        var viewModel = CreatePaginatedContentListBox();
        var count = 0;
        var testModelList = new List<TestModel>();
        while (count < 30)
        {
            testModelList.Add(new TestModel { Name = $"TestModel{count + 1}" });
            count++;
        }

        viewModel.FullContent = testModelList;

        viewModel.ShiftToNextPage.Execute().Subscribe();
        viewModel.ShiftToNextPage.Execute().Subscribe();

        viewModel.Invoking(vm => vm.ShiftToNextPage.Execute().Subscribe()).Should()
            .Throw<InvalidPageRequestException>().WithMessage("Attempted to shift to next page from final page");
    }

    [Test]
    public void WillNotShitToPreviousPageFromFirstPage()
    {
        var viewModel = CreatePaginatedContentListBox();

        viewModel.Invoking(vm => vm.ShiftToPreviousPage.Execute().Subscribe()).Should()
            .Throw<InvalidPageRequestException>().WithMessage("Attempted to shift to previous page from first page");
    }

    [Test]
    public void SpecifiesDefaultNoContentMessageControlName()
    {
        var viewModel = CreatePaginatedContentListBox();

        viewModel.NoContentMessageControlName.Should().Be("NoContentMessage");
    }
}