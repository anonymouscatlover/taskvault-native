// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.ViewModels;

namespace TaskVaultNative.Tests.Client.Unit.Services;

[TestFixture]
public class EmbeddedViewModelFactoryAssertions
{
    public EmbeddedViewModelFactory CreateEmbeddedViewModelFactory()
    {
        return new(new TestRoutableViewModelFactory(), new TestApplicationDataStorageService());
    }

    [Test]
    public void CanCreateEmbeddedDetailsScreen()
    {
        var service = CreateEmbeddedViewModelFactory();

        var viewModel = service.CreateEmbeddedDetailsScreenViewModel();

        viewModel.Should().BeOfType<EmbeddedDetailsScreenViewModel>();
    }

    [Test]
    public void CanCreateVaultHeaderMenuViewModel()
    {
        var service = CreateEmbeddedViewModelFactory();

        var viewModel = service.CreateVaultHeaderMenuViewModel(new TestScreen());

        viewModel.Should().BeOfType<VaultHeaderMenuViewModel>();
    }

    [Test]
    public void CanCreateNewProjectGroupFormViewModel()
    {
        var service = CreateEmbeddedViewModelFactory();

        var viewModel = service.CreateNewProjectGroupFormViewModel(new TestScreen());

        viewModel.Should().BeOfType<NewProjectGroupFormViewModel>();
    }

    [Test]
    public void CreatePaginatedContentListBoxViewModel()
    {
        var service = CreateEmbeddedViewModelFactory();

        var viewModel = service.CreatePaginatedContentListBoxViewModel<TestModel>(new TestScreen());

        viewModel.Should().BeOfType<PaginatedContentListBoxViewModel<TestModel>>();
    }

    [Test]
    public void CreateProjectGroupDetailsViewModel()
    {
        var service = CreateEmbeddedViewModelFactory();

        var viewModel = service.CreateProjectGroupDetailsViewModel(new TestScreen());

        viewModel.Should().BeOfType<ProjectGroupDetailsViewModel>();
    }
    /*
     Note: determined these tests are redundant so stopped adding; for more info see note at the bottom of RoutableViewModelFactoryAssertions.cs
     */
}