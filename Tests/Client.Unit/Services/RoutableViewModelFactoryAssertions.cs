// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.ViewModels;

namespace TaskVaultNative.Client.Services.RoutableViewModelFactory;

[TestFixture]
public class RoutableViewModelFactoryAssertions
{
    public RoutableViewModelFactory CreateRoutableViewModelFactory()
    {
        return new(new TestLocalVaultBuilderService(), new TestApplicationDataStorageService(), new TestBitmapAssetValueConverter());
    }

    [Test]
    public void CanCreateLocalVaultSelectViewModel()
    {
        var service = CreateRoutableViewModelFactory();

        var viewModel = service.CreateLocalVaultSelectViewModel(new TestScreen());

        viewModel.Should().BeOfType<LocalVaultSelectViewModel>();
    }

    [Test]
    public void CanCreateNewLocalVaultFormViewModel()
    {
        var service = CreateRoutableViewModelFactory();

        var viewModel = service.CreateNewLocalVaultFormViewModel(new TestScreen());

        viewModel.Should().BeOfType<NewLocalVaultFormViewModel>();
    }

    [Test]
    public void CanCreateEncryptionAtRestFormViewModel()
    {
        var service = CreateRoutableViewModelFactory();

        var viewModel = service.CreateEncryptionAtRestFormViewModel(new TestScreen());

        viewModel.Should().BeOfType<EncryptionAtRestFormViewModel>();
    }

    [Test]
    public void CanCreateConnectToRemoteFormViewModel()
    {
        var service = CreateRoutableViewModelFactory();

        var viewModel = service.CreateConnectToRemoteFormViewModel(new TestScreen());

        viewModel.Should().BeOfType<ConnectToRemoteFormViewModel>();
    }

    [Test]
    public void CanCreateMainMenuViewModel()
    {
        var service = CreateRoutableViewModelFactory();

        var viewModel = service.CreateMainMenuViewModel(new TestScreen());

        viewModel.Should().BeOfType<MainMenuViewModel>();
    }

    [Test]
    public void CanCreateProjectGroupsViewModel()
    {
        var service = CreateRoutableViewModelFactory();

        var viewModel = service.CreateProjectGroupsViewModel(new TestScreen());

        viewModel.Should().BeOfType<ProjectGroupsViewModel>();
    }

    [Test]
    public void CanCreateProjectsViewModel()
    {
        var service = CreateRoutableViewModelFactory();

        var viewModel = service.CreateProjectsViewModel(new TestScreen());

        viewModel.Should().BeOfType<ProjectsViewModel>();
    }

    /* note: ceased to add additional tests to this suite after ProjectsViewModel;
        This functionality is tested indirectly through UI Tests and to a lesser extent, Backend Integration tests:
        if this service does not correctly create a requested ViewModel, the expected View will not load (causing UI test failures)
        and of course ViewModel routing will fail for that ViewModel, so any relevant ViewModelFlow Backend Integration tests will fail as well 
     */
}