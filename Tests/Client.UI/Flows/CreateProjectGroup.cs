// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using Avalonia.VisualTree;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
[NonParallelizable]
public class CreateProjectGroup : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        _dataGenerator.AddTestLocalVault();
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task CreateFirstProjectGroup()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var noLocalVaultsMessage = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoLocalVaultsMessage");
            noLocalVaultsMessage.IsVisible.Should().BeFalse();

            var localVaultListItems = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>();
            localVaultListItems.Count().Should().Be(1);

            var localVaultListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Local Vault"));
            localVaultListItem.IsSelected = true;

            var openVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenVaultButton");
            openVaultButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ProjectGroupsButton");
            projectGroupsButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectGroupsView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupsHeaderTextBlock = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "ProjectGroupsHeader");
            projectGroupsHeaderTextBlock.Text.Should().NotBeEmpty();

            var newProjectGroupButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NewProjectGroupButton");
            newProjectGroupButton.Content.Should().Be("New Project Group");

            var noProjectGroupsMessage = MainWindow.GetVisualDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoProjectGroupsMessage");
            noProjectGroupsMessage.IsVisible.Should().BeTrue();
            noProjectGroupsMessage.Text.Should().NotBeEmpty();

            var backButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "BackButton");
            backButton.Content.Should().Be("Back");
            backButton.Command.Should().NotBeNull();

            var nextPageOfProjectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NextPageButton");
            nextPageOfProjectGroupsButton.Content.Should().Be("Next");
            nextPageOfProjectGroupsButton.IsEffectivelyEnabled.Should().BeFalse();

            var previousPageOfProjectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "PreviousPageButton");
            previousPageOfProjectGroupsButton.Content.Should().Be("Previous");
            previousPageOfProjectGroupsButton.IsEffectivelyEnabled.Should().BeFalse();

            var detailsView = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsView.Should().NotBeNull();

            var noContentMessage = detailsView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noContentMessage.Text.Should().NotBeEmpty();

            newProjectGroupButton.Command.Execute(null);

            var newProjectGroupFormView = MainWindow.GetLogicalDescendants().OfType<NewProjectGroupFormView>().Single();
            newProjectGroupFormView.Should().NotBeNull();

            var newProjectGroupHeader = newProjectGroupFormView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormHeader");
            newProjectGroupHeader.Text.Should().NotBeEmpty();

            var submitButton = newProjectGroupFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "SubmitButton");
            submitButton.Content.Should().Be("Submit");

            var cancelButton = newProjectGroupFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "CancelButton");
            cancelButton.Content.Should().Be("Cancel");
            cancelButton.Command.Should().NotBeNull();

            var projectGroupsListBox = MainWindow.GetLogicalDescendants().OfType<ListBox>()
                .Single(listBox => listBox.Name == "ProjectGroupsListBox");
            projectGroupsListBox.SelectionMode.Should().Be(SelectionMode.Single);
            projectGroupsListBox.IsVisible.Should().BeFalse();

            var projectGroupNameField = newProjectGroupFormView.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "ProjectGroupNameField");
            projectGroupNameField.Watermark.Should().Be("Project Group Name");

            var formErrorMessages = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormErrorMessages");
            formErrorMessages.Text.Should().BeNullOrEmpty();
            submitButton.Command.Execute(null);
            formErrorMessages.Text.Should().NotBeEmpty();

            projectGroupNameField.Text = "UI Test Project Group";
            formErrorMessages.Text.Should().BeEmpty();
            submitButton.Command.Execute(null);
            MainWindow.GetVisualDescendants().OfType<NewProjectGroupFormView>().Count().Should().Be(0);
        });

        await Task.Delay(1000);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupsListBox = MainWindow.GetLogicalDescendants().OfType<ListBox>()
                .Single(listBox => listBox.Name == "ProjectGroupsListBox");

            var noProjectGroupsMessage = MainWindow.GetVisualDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoProjectGroupsMessage");
            var numberOfProjectGroupsDEBUG = projectGroupsListBox.GetLogicalDescendants().OfType<ListBoxItem>();
            numberOfProjectGroupsDEBUG.Count().Should().Be(1);
            noProjectGroupsMessage.IsVisible.Should().BeFalse();
            projectGroupsListBox.IsVisible.Should().BeTrue();

            var projectGroupListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "UI Test Project Group"));
            projectGroupListItem.Should().NotBeNull();
        });
    }
}