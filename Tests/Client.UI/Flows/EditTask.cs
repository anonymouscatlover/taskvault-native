// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.EmbeddedViews;
using TaskVaultNative.Client.Views.TopLevelViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
public class EditTask : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        _dataGenerator.AddTestLocalVault();
        _dataGenerator.AddProjectGroupForLocalVault("Test Local Vault");
        _dataGenerator.AddProjectToProjectGroup("Test Project Group");
        _dataGenerator.AddTaskToProject();
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task EditExistingTaskWithNoContexts()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var localVaultListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Local Vault"));
            localVaultListItem.IsSelected = true;

            var openVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenVaultButton");
            openVaultButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ProjectGroupsButton");
            projectGroupsButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectGroupsView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project Group"));
            projectGroupListItem.Should().NotBeNull();
            projectGroupListItem.IsSelected = true;

            var openProjectGroupButton = projectGroupListItem.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenButton");
            openProjectGroupButton.Content.Should().Be("Open");

            openProjectGroupButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectsView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var detailsScreen = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsScreen.Should().NotBeNull();

            var projectListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>().Single(listBoxItem =>
                listBoxItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project"));
            projectListItem.Should().NotBeNull();
            projectListItem.IsSelected = true;

            var openProjectButton = projectListItem.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenButton");
            openProjectButton.Content.Should().Be("Open");

            openProjectButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<TasksView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var taskListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>().Single(listBoxItem =>
                listBoxItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Task"));
            taskListItem.Should().NotBeNull();

            taskListItem.IsSelected = true;

            var taskDetailsView = MainWindow.GetLogicalDescendants().OfType<TaskDetailsView>().Single();

            var editButton = taskDetailsView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "EditButton");

            editButton.Command.Execute(null);
        });

        await Task.Delay(500);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var editTaskFormView =
                MainWindow.GetLogicalDescendants().OfType<EditTaskFormView>().Single();
            editTaskFormView.Should().NotBeNull();

            var editTaskHeader = editTaskFormView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormHeader");
            editTaskHeader.Text.Should().Be("Editing Task - Test Task");

            var taskNameField = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "TaskNameField");
            taskNameField.Text.Should().NotBeEmpty();

            taskNameField.Text = "Renamed Test Task";

            var taskContextField = editTaskFormView.GetLogicalDescendants().OfType<ListBox>()
                .Single(listBox => listBox.Name == "TaskContextField");
            taskContextField.IsVisible.Should().BeFalse();

            var noTaskContextsMessage = editTaskFormView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoTaskContextsMessage");
            noTaskContextsMessage.IsVisible.Should().BeTrue();

            var taskDescriptionField = editTaskFormView.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "TaskDescriptionField");
            taskDescriptionField.Text.Should().NotBeEmpty();

            taskDescriptionField.Text = "Modified Task description";

            var submitButton = editTaskFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "SubmitButton");
            submitButton.Content.Should().Be("Submit");

            submitButton.Command.Execute(null);

            var queryForOriginalTaskName = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Where(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Task"));
            queryForOriginalTaskName.Should().BeEmpty();

            var modifiedTask = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Renamed Test Task"));

            modifiedTask.IsSelected = true;

            var modifiedTaskDescription = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "TaskDescription");
            modifiedTaskDescription.Text.Should().Be("Modified Task description");
        });
    }
}