// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using Avalonia.VisualTree;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.EmbeddedViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
[NonParallelizable]
public class CreateLocalVault : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task CreateLocalVaultWithNoAdditionalConfigurations()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            VerifyViewIsActive<LocalVaultSelectView>();

            var logo = MainWindow.GetVisualDescendants().OfType<Image>()
                .Single(image => image.Name == "TaskVaultLogo");
            logo.Should().NotBeNull();

            var copyright = MainWindow.GetVisualDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "Copyright").Text;
            copyright.Should().NotBeEmpty();

            var licenseLogo = MainWindow.GetLogicalDescendants().OfType<Image>()
                .Single(image => image.Name == "GplV3Logo");
            licenseLogo.IsEffectivelyVisible.Should().BeTrue();
            
            var version = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "Version");
            version.Text.Should().NotBeEmpty();
            
            var newVaultButton = MainWindow.GetVisualDescendants().OfType<Button>()
                .Single(button => button.Name == "NewLocalVaultButton");
            newVaultButton.Content.Should().Be("New Local Vault");

            var noLocalVaultsMessage = MainWindow.GetVisualDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoLocalVaultsMessage");
            noLocalVaultsMessage.IsVisible.Should().BeTrue();
            noLocalVaultsMessage.Text.Should().NotBeEmpty();

            var localVaultsListBox = MainWindow.GetLogicalDescendants().OfType<ListBox>()
                .Single(listbox => listbox.Name == "LocalVaultsListBox");
            localVaultsListBox.IsVisible.Should().BeFalse();

            var numberOfVaultsDEBUG = localVaultsListBox.GetLogicalDescendants().OfType<ListBoxItem>();
            numberOfVaultsDEBUG.Count().Should().Be(0);

            NavigateToNewLocalVaultFormView();
        });

        await ViewWasLoadedCondition<NewLocalVaultFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            VerifyViewIsActive<NewLocalVaultFormView>();

            var viewHeader = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "ViewHeader");
            viewHeader.Text.Should().NotBeEmpty();

            var cancelButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "CancelButton");
            cancelButton.Command.Should().NotBeNull();

            var nextButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NextButton");
            nextButton.Content.Should().Be("Next");

            var vaultNameField = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "VaultNameField");
            vaultNameField.Watermark.Should().NotBeEmpty();

            var formErrorMessages = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormErrorMessages");
            formErrorMessages.Text.Should().BeNullOrEmpty();
            nextButton.Command.Execute(null);
            formErrorMessages.Text.Should().NotBeEmpty();

            vaultNameField.Text = "Test Vault Name";
            nextButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<EncryptionAtRestFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            VerifyViewIsActive<EncryptionAtRestFormView>();

            var viewHeader = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "ViewHeader");
            viewHeader.Text.Should().NotBeEmpty();

            var cancelButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "CancelButton");
            cancelButton.Content.Should().Be("Cancel");
            cancelButton.Command.Should().NotBeNull();

            var backButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "BackButton");
            backButton.Content.Should().Be("Back");
            backButton.Command.Should().NotBeNull();

            var submitButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "SubmitButton");
            submitButton.Content.Should().Be("Submit");

            var passwordField = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "PasswordField");
            passwordField.Watermark.Should().NotBeEmpty();

            var confirmPasswordField = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "ConfirmPasswordField");
            confirmPasswordField.Watermark.Should().NotBeEmpty();

            var encryptionAtRestDescription = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBox => textBox.Name == "EncryptionAtRestDescription");
            encryptionAtRestDescription.Text.Should().NotBeNullOrEmpty();
            encryptionAtRestDescription.IsVisible.Should().BeFalse();

            var encryptionAtRestDescriptionToggle = MainWindow.GetLogicalDescendants().OfType<ToggleButton>()
                .Single(textBox => textBox.Name == "EncryptionAtRestDescriptionToggle");
            encryptionAtRestDescriptionToggle.IsChecked = true;
            encryptionAtRestDescription.IsVisible.Should().BeTrue();

            var declineButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "DeclineButton");
            declineButton.Content.Should().Be("Decline");
            declineButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ConnectToRemoteFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            VerifyViewIsActive<ConnectToRemoteFormView>();

            var viewHeader = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "ViewHeader");
            viewHeader.Text.Should().NotBeEmpty();

            var remoteVaultsDescription = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBox => textBox.Name == "RemoteVaultsDescription");
            remoteVaultsDescription.Text.Should().NotBeNullOrEmpty();
            remoteVaultsDescription.IsVisible.Should().BeFalse();

            var remoteVaultsDescriptionToggle = MainWindow.GetLogicalDescendants().OfType<ToggleButton>()
                .Single(textBox => textBox.Name == "RemoteVaultsDescriptionToggle");
            remoteVaultsDescriptionToggle.IsChecked = true;
            remoteVaultsDescription.IsVisible.Should().BeTrue();

            var cancelButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "CancelButton");
            cancelButton.Content.Should().Be("Cancel");
            cancelButton.Command.Should().NotBeNull();

            var backButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "BackButton");
            backButton.Content.Should().Be("Back");
            backButton.Command.Should().NotBeNull();

            var registerNewRemoteVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "RegisterNewRemoteVaultButton");
            registerNewRemoteVaultButton.Content.Should().Be("Register New Remote Vault");

            var connectToExistingRemoteVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ConnectToExistingRemoteVaultButton");
            connectToExistingRemoteVaultButton.Content.Should().Be("Connect to Existing Remote Vault");

            var declineButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "DeclineButton");
            declineButton.Content.Should().Be("Decline");
            declineButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            VerifyViewIsActive<MainMenuView>();
            var viewHeader = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "ViewHeader");
            viewHeader.Text.Should().NotBeEmpty();
            viewHeader.Text.Should().EndWith("- Test Vault Name");

            var headerMenu = MainWindow.GetLogicalDescendants().OfType<Menu>()
                .Single(menu => menu.Name == "HeaderMenu");

            var navigateSubMenu = headerMenu.Items.OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "NavigationSubMenu");
            navigateSubMenu.Header.Should().Be("Navigate");

            var inboxMenuItem = navigateSubMenu.Items.OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "InboxMenuItem");
            inboxMenuItem.Header.Should().Be("Inbox");

            var mainMenuMenuItem = navigateSubMenu.Items.OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "MainMenuMenuItem");
            mainMenuMenuItem.Header.Should().Be("Main Menu");
            mainMenuMenuItem.Command.Should().NotBeNull();

            var optionsSubMenu = headerMenu.Items.OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "OptionsSubMenu");
            optionsSubMenu.Header.Should().Be("Options");

            var lockMenuItem = optionsSubMenu.Items.OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "LockMenuItem");
            lockMenuItem.Header.Should().Be("Lock Vault");

            var vaultSettingsMenuItem = optionsSubMenu.Items.OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "VaultSettingsMenuItem");
            vaultSettingsMenuItem.Header.Should().Be("Vault Settings");

            var switchVaultsMenuItem = optionsSubMenu.Items.OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "SwitchVaultsMenuItem");
            switchVaultsMenuItem.Header.Should().Be("Switch Vaults");

            var helpSubMenu = headerMenu.Items.OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "HelpSubMenu");
            helpSubMenu.Header.Should().Be("Help");

            var inboxButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "InboxButton");
            inboxButton.Content.Should().Be("Inbox");

            var todayButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "TodayButton");
            todayButton.Content.Should().Be("Today");
            todayButton.IsEnabled.Should().BeFalse();

            var thisWeekButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ThisWeekButton");
            thisWeekButton.Content.Should().Be("This Week");
            thisWeekButton.IsEnabled.Should().BeFalse();

            var projectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ProjectGroupsButton");
            projectGroupsButton.Content.Should().Be("Project Groups");

            var actionListsListBox = MainWindow.GetLogicalDescendants().OfType<PaginatedContentListBoxView>()
                .Single(listBox => listBox.Name == "ActionListsListBox");
            var noContentMessage = actionListsListBox.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noContentMessage.Text.Should().NotBeNullOrEmpty();

            var newActionListButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NewActionListButton");
            newActionListButton.Content.Should().Be("New Action List");

            switchVaultsMenuItem.Command.Execute(null);
        });

        await Task.Delay(1000); // wait needed to ensure local vaults have been loaded from database
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            VerifyViewIsActive<LocalVaultSelectView>();

            var nextPageOfVaultsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NextPageButton");
            nextPageOfVaultsButton.Content.Should().Be("Next");
            nextPageOfVaultsButton.IsEffectivelyEnabled.Should().BeFalse();

            var previousPageOfVaultsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "PreviousPageButton");
            previousPageOfVaultsButton.Content.Should().Be("Previous");
            previousPageOfVaultsButton.IsEffectivelyEnabled.Should().BeFalse();

            var openVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenVaultButton");
            openVaultButton.Content.Should().Be("Open Vault");
            openVaultButton.IsEffectivelyEnabled.Should().BeFalse();

            var localVaultsListBox = MainWindow.GetLogicalDescendants().OfType<ListBox>()
                .Single(listbox => listbox.Name == "LocalVaultsListBox");
            localVaultsListBox.SelectionMode.Should().Be(SelectionMode.Single);
            localVaultsListBox.IsVisible.Should().BeTrue();

            var numberOfVaultsDEBUG = localVaultsListBox.GetLogicalDescendants().OfType<ListBoxItem>();
            numberOfVaultsDEBUG.Count().Should().Be(1);

            var noLocalVaultsMessage = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoLocalVaultsMessage");
            noLocalVaultsMessage.IsVisible.Should().BeFalse();

            var createdLocalVault = localVaultsListBox.GetLogicalDescendants().OfType<TextBlock>()
                .Where(textBlock => textBlock.Name == "LocalVaultName")
                .Where(textBlock => textBlock.Text == "Test Vault Name"); //TODO : figure out why Single is failing
            createdLocalVault.Should().NotBeNull();

            var createdLocalVaultListItem = localVaultsListBox.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Vault Name"));
            createdLocalVaultListItem.IsSelected = true;
            openVaultButton.IsEnabled.Should().BeTrue();

            openVaultButton.Command.Execute(null);

            VerifyViewIsActive<MainMenuView>();
        });
    }

    private void NavigateToNewLocalVaultFormView()
    {
        var newVaultButton = MainWindow.GetVisualDescendants().OfType<Button>()
            .Single(button => button.Name == "NewLocalVaultButton");
        newVaultButton.Command.Execute(null);
    }
}