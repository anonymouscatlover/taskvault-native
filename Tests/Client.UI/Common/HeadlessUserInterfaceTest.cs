// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Threading;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI;

[TestFixture]
[NonParallelizable]
public class HeadlessUserInterfaceTest
{
    [OneTimeSetUp]
    public async Task SuiteSetup()
    {
        await WaitforAppToBeReady();
        ConfigureCoreTestComponents();
        _dataGenerator = new TestDataGenerator();
    }

    [SetUp]
    public async Task ResetAppState()
    {
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var mainWindowViewModel = MainWindow.ViewModel;
            mainWindowViewModel.ResetAppForUiTesting.Execute().Subscribe();
        });
    }

    protected IClassicDesktopStyleApplicationLifetime App { get; private set; }
    protected MainWindow MainWindow { get; private set; }
    protected TestDataGenerator _dataGenerator { get; private set; }

    protected static Task RunTestInUiThread(Action spec)
    {
        return Dispatcher.UIThread.InvokeAsync(() => spec);
    }

    private async Task WaitforAppToBeReady()
    {
        await AppReadyCondition.CheckAsync();
    }

    private void ConfigureCoreTestComponents()
    {
        App = AvaloniaAppUnderTest.GetApp();
        MainWindow = AvaloniaAppUnderTest.GetMainWindow();
    }
}