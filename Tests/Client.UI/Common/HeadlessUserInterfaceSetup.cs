// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Threading;
using NUnit.Framework;

namespace TaskVaultNative.Tests.Client.UI;

[SetUpFixture]
public class HeadlessUserInterfaceSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        SpawnAppUnderTestThread();
    }

    private void SpawnAppUnderTestThread()
    {
        var tcs = new TaskCompletionSource<SynchronizationContext>();
        var thread = new Thread(() =>
        {
            try
            {
                AvaloniaAppUnderTest
                    .BuildAvaloniaApp()
                    .AfterSetup(_ => { tcs.SetResult(SynchronizationContext.Current); })
                    .StartWithClassicDesktopLifetime(new string[0]);

                Dispatcher.UIThread.MainLoop(CancellationToken.None);
            }
            catch (Exception e)
            {
                tcs.SetException(e);
            }
        })
        {
            IsBackground = true
        };

        thread.Start();

        SynchronizationContext.SetSynchronizationContext(tcs.Task.Result);
    }

    [OneTimeTearDown]
    public void TearDown()
    {
        AvaloniaAppUnderTest.Stop();
        DeleteDatabase();
    }

    private void DeleteDatabase()
    {
        if (File.Exists(Path.Join(Directory.GetCurrentDirectory(), "TaskVault", "TaskVaultTest.db")))
            File.Delete(Path.Join(Directory.GetCurrentDirectory(), "TaskVault", "TaskVaultTest.db"));
    }
}