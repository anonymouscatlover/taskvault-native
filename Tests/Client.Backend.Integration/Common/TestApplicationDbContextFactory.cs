// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client;

public class TestApplicationDbContextFactory : IDbContextFactory<ApplicationDbContext>
{
    public ApplicationDbContext CreateDbContext()
    {
        IConfigurationBuilder builder = new ConfigurationBuilder();
        builder = builder.SetBasePath(Directory.GetCurrentDirectory());
        var configFilename = "appsettings.test.json";
        if (!File.Exists(Path.Join(Directory.GetCurrentDirectory(), configFilename)))
            Console.WriteLine("Test config file does not exist!");
        builder = builder.AddJsonFile(configFilename, false, true);
        var config = builder.Build();

        var dbContext = new ApplicationDbContext(config);

        return dbContext;
    }
}