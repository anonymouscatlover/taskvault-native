// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Splat;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Tests.Client.Backend.Integration.Common;

[TestFixture]
public class IntegrationTest
{
    [SetUp]
    public void Setup()
    {
        ConfigureAppTestConfiguration();
        ConfigureTestServiceContainer();
        ConfigureApplicationDatabase();
    }

    [TearDown]
    public void TearDown()
    {
        DeleteDatabase();
    }

    protected IConfiguration? _appTestConfiguration;
    private ILogger? _logger;
    protected IServiceProvider _testServiceProvider;

    private void ConfigureAppTestConfiguration()
    {
        _appTestConfiguration = new AppConfigurationLoader("appsettings.test.json").AppConfiguration;
    }

    private void ConfigureTestServiceContainer()
    {
        _testServiceProvider = new ServiceContainer(_appTestConfiguration);
    }

    private void ConfigureApplicationDatabase()
    {
        using (var serviceScope = _testServiceProvider.CreateScope())
        {
            var context = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();
        }
    }

    private void DeleteDatabase()
    {
        using (var serviceScope = _testServiceProvider.CreateScope())
        {
            var context = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            context.Database.EnsureDeleted();
        }
    }
}