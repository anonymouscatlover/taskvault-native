// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;
using TaskVaultNative.Client.ViewModels;

namespace TaskVaultNative.Client.Views;

public partial class NewLocalVaultFormView : ReactiveUserControl<NewLocalVaultFormViewModel>
{
    public NewLocalVaultFormView()
    {
        this.WhenActivated(disposables =>
        {
            /*
            this.BindValidation(ViewModel, vm => vm.LocalVaultName, view => view.VaultNameField.Text)
                .DisposeWith(disposables);
                */
        });
        InitializeComponent();

#if DEBUG
        // TODO: this code does not work
        var localVaultNameField = this.FindControl<TextBox>("VaultNameField");
        var autofillButton = new Button();
        autofillButton.Command = ReactiveCommand.Create(() => ViewModel.LocalVaultName = "Valid Vault Name");
        HotKeyManager.SetHotKey(autofillButton, new KeyGesture(Key.LeftAlt));
#endif
    }

    private void InitializeComponent()
    {
        AvaloniaXamlLoader.Load(this);
    }
}