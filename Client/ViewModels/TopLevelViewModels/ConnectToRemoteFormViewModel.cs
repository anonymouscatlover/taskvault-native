// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive;
using System.Reactive.Disposables;
using ReactiveUI;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;

namespace TaskVaultNative.Client.ViewModels;

public class ConnectToRemoteFormViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private readonly IApplicationDataStorageService _dataStorage;
    private readonly ILocalVaultBuilderService _localVaultBuilder;

    private bool _remoteVaultsDescriptionVisible;
    private readonly IRoutableViewModelFactory _routableViewModelFactory;

    public ConnectToRemoteFormViewModel(IScreen screen, IRoutableViewModelFactory routableViewModelFactory,
        IApplicationDataStorageService dataStorage,
        ILocalVaultBuilderService localVaultBuilder)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _dataStorage = dataStorage;
        _localVaultBuilder = localVaultBuilder;
        _routableViewModelFactory = routableViewModelFactory;

        NavigateBack = ReactiveCommand.Create(() => { HostScreen.Router.NavigateBack.Execute(); });

        CancelProcess = ReactiveCommand.CreateFromObservable(() => HostScreen.Router.NavigateAndReset
            .Execute(routableViewModelFactory.CreateLocalVaultSelectViewModel(HostScreen)));

        Decline = ReactiveCommand.Create(RespondToDecline);

        NavigateToMainMenu = ReactiveCommand.CreateFromObservable(PrepareMainMenuViewModel);

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public ReactiveCommand<Unit, Unit> NavigateBack { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> CancelProcess { get; }
    public ReactiveCommand<Unit, Unit> Decline { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToMainMenu { get; }
    public ReactiveCommand<Unit, Unit> PersistVaultBuild { get; }

    public string? HeaderPrompt { get; } = "Connect this local vault to a remote vault?";

    public string? RemoteVaultsDescription { get; } =
        "\"Remote Vaults\" are a mechanism for storing TaskVault user data online, in order to protect against data loss and enable data synchronization " +
        "between user devices. All data is encrypted client-side before being sent to a remote vault (end-to-end-encryption). Only one remote-vault " +
        "can be connected to a local-vault, but many local-vaults can be connected to the same remote-vault, which enables data synchronization. ";

    public bool RemoteVaultsDescriptionVisible
    {
        get => _remoteVaultsDescriptionVisible;
        set => this.RaiseAndSetIfChanged(ref _remoteVaultsDescriptionVisible, value);
    }

    public ViewModelActivator Activator { get; }

    public string UrlPathSegment { get; } = "connect-to-remote-form";
    public IScreen HostScreen { get; }

    public void RespondToDecline()
    {
        var newVault = _localVaultBuilder.NewLocalVaultBuild;
        _dataStorage.SaveContent(newVault);
        NavigateToMainMenu.Execute();
    }

    private IObservable<IRoutableViewModel> PrepareMainMenuViewModel()
    {
        var mainMenuViewModel =
            _routableViewModelFactory.CreateMainMenuViewModel(HostScreen, _localVaultBuilder.NewLocalVaultBuild);

        return HostScreen.Router.NavigateAndReset
            .Execute(mainMenuViewModel);
    }
}