// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;

namespace TaskVaultNative.Client.ViewModels;

public class LocalVaultSelectViewModel : ViewModelBase, IRoutableViewModel, IActivatableViewModel
{
    private readonly IApplicationDataStorageService _applicationDataStorage;
    private readonly IEmbeddedViewModelFactory _embeddedViewModelFactory;

    private readonly List<LocalVaultModel> _loadedLocalVaults;

    private readonly IRoutableViewModelFactory _routableViewModelFactory;

    private readonly ObservableAsPropertyHelper<LocalVaultModel> _selectedLocalVault;

    private int _vaultsPerPage = 10;

    public LocalVaultSelectViewModel(IScreen screen, IRoutableViewModelFactory routableViewModelFactory,
        IApplicationDataStorageService applicationDataStorage,
        IEmbeddedViewModelFactory embeddedViewModelFactory)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _routableViewModelFactory = routableViewModelFactory;
        _applicationDataStorage = applicationDataStorage;
        _embeddedViewModelFactory = embeddedViewModelFactory;

        _loadedLocalVaults = (List<LocalVaultModel>)_applicationDataStorage.LoadLocalVaults();

        LocalVaultsPaginatedList =
            _embeddedViewModelFactory.CreatePaginatedContentListBoxViewModel<LocalVaultModel>(HostScreen);
        // TODO : improve this implementation for Unit testing
        if (LocalVaultsPaginatedList != null)
        {
            LocalVaultsPaginatedList.NoContentMessage = NoLocalVaultsMessage;
            LocalVaultsPaginatedList.NoContentMessageControlName = NoLocalVaultsMessageControlName;
            LocalVaultsPaginatedList.Name = LocalVaultsListBoxControlName;
            LocalVaultsPaginatedList.FullContent = _loadedLocalVaults;
        }

        _selectedLocalVault = this.WhenAnyValue(vm => vm.LocalVaultsPaginatedList.SelectedItem)
            .ToProperty(this, vm => vm.SelectedLocalVault);

        var vaultIsSelected = this.WhenAnyValue(vm => vm.SelectedLocalVault)
            .Select(vault => vault != null);
        OpenSelectedVault = ReactiveCommand.Create(() => RespondToOpenSelectedVault(), vaultIsSelected);

        NavigateToMainMenu = ReactiveCommand.CreateFromObservable(PrepareMainMenuViewModel);

        NavigateToNewLocalVaultFormView =
            ReactiveCommand.CreateFromObservable(() =>
                HostScreen.Router.Navigate.Execute(
                    _routableViewModelFactory.CreateNewLocalVaultFormViewModel(HostScreen)));

        NavigateToAbout = ReactiveCommand.CreateFromObservable(() =>
            HostScreen.Router.Navigate.Execute(_routableViewModelFactory.CreateAboutViewModel(HostScreen)));

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public PaginatedContentListBoxViewModel<LocalVaultModel> LocalVaultsPaginatedList { get; }

    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToNewLocalVaultFormView { get; }
    public ReactiveCommand<Unit, Unit> OpenSelectedVault { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToMainMenu { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToAbout { get; }
    public LocalVaultModel SelectedLocalVault => _selectedLocalVault.Value;

    public string? AppTitle { get; } = "TaskVault";
    public string? NoLocalVaultsMessage { get; } = "No local vaults found. Create a vault to get started.";

    public string? Copyright { get; } = "Copyright 2022 Tyler Hasty";
    
    public string Version { get; } = "v0.1.0";
    
    public string TaskVaultLogoSource { get; } = Path.ChangeExtension(Path.Join("/Assets", "taskvault-logo"), ".png");
    public string GplV3LogoSource { get; } = Path.ChangeExtension(Path.Join("/Assets", "gplv3-with-text-136x68"), ".png");

    public string NoLocalVaultsMessageControlName { get; } = "NoLocalVaultsMessage";

    public string LocalVaultsListBoxControlName { get; } = "LocalVaultsListBox";
    public ViewModelActivator Activator { get; }

    public string UrlPathSegment { get; } = "local-vault-select";
    public IScreen HostScreen { get; }

    private void RespondToOpenSelectedVault()
    {
        NavigateToMainMenu.Execute().Subscribe();
    }

    private IObservable<IRoutableViewModel> PrepareMainMenuViewModel()
    {
        var mainMenuViewModel =
            _routableViewModelFactory.CreateMainMenuViewModel(HostScreen, LocalVaultsPaginatedList.SelectedItem);


        return HostScreen.Router.Navigate.Execute(mainMenuViewModel);
    }
}