// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;

namespace TaskVaultNative.Client.ViewModels;

public class ProjectsViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private ProjectGroupModel _activeProjectGroup;
    private readonly IApplicationDataStorageService _applicationDataStorage;

    private readonly IEmbeddedViewModelFactory _embeddedViewModelFactory;
    private string _header = "Projects";

    private readonly string _headerPrefix = "Projects - ";

    private IEnumerable<ProjectModel> _loadedProjects = new List<ProjectModel>();
    private readonly IRoutableViewModelFactory _routableViewModelFactory;

    public ProjectsViewModel(IScreen screen, IEmbeddedViewModelFactory embeddedViewModelFactory,
        IRoutableViewModelFactory routableViewModelFactory, IApplicationDataStorageService applicationDataStorage,
        ProjectGroupModel activeProjectGroup = null)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _applicationDataStorage = applicationDataStorage;
        _embeddedViewModelFactory = embeddedViewModelFactory;
        _routableViewModelFactory = routableViewModelFactory;
        ActiveProjectGroup = activeProjectGroup ?? new ProjectGroupModel();

        DetailsScreen = _embeddedViewModelFactory.CreateEmbeddedDetailsScreenViewModel();
        HeaderMenu = _embeddedViewModelFactory
            .CreateVaultHeaderMenuViewModel(HostScreen,
                _applicationDataStorage.LoadLocalVaultForProjectGroup(ActiveProjectGroup));
        ProjectsPaginatedList =
            _embeddedViewModelFactory.CreatePaginatedContentListBoxViewModel<ProjectModel>(HostScreen);

        this.WhenAnyValue(vm => vm.ProjectsPaginatedList)
            .WhereNotNull()
            .Do(paginatedList => paginatedList.NoContentMessage = NoProjectsMessage)
            .Do(paginatedList => paginatedList.Name = ProjectsListBoxControlName)
            .Do(_ => ReloadProjects())
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(NewProjectFormViewModel))
            .Cast<NewProjectFormViewModel>()
            .Select(newProjectFormViewModel => newProjectFormViewModel.SubmitForm)
            .Select(command => command.IsExecuting)
            .Switch()
            .Do(_ => ReloadProjects())
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(ProjectDetailsViewModel))
            .Cast<ProjectDetailsViewModel>()
            .Do(projectDetailsViewModel => projectDetailsViewModel.OpenEditProjectForm = OpenEditProjectForm)
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(ProjectDetailsViewModel))
            .Cast<ProjectDetailsViewModel>()
            .Do(projectDetailsViewModel => projectDetailsViewModel.DeleteProject = DeleteSelectedProject)
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(EditProjectFormViewModel))
            .Cast<EditProjectFormViewModel>()
            .Select(editProjectFormViewModel => editProjectFormViewModel.SubmitForm)
            .Select(command => command.IsExecuting)
            .Switch()
            .Do(_ => ReloadProjects())
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(EditProjectFormViewModel))
            .Cast<EditProjectFormViewModel>()
            .Do(editProjectGroupFormViewModel =>
                editProjectGroupFormViewModel.TargetProject = ProjectsPaginatedList.SelectedItem)
            .Subscribe();

        this.WhenAnyValue(vm => vm.ActiveProjectGroup)
            .Select(projectGroup => projectGroup.Name)
            .Do(name => Header = $"{_headerPrefix}{name}")
            .Subscribe();

        OpenProjectDetails = ReactiveCommand.CreateFromObservable(() =>
            DetailsScreen.Router.NavigateAndReset.Execute(
                _embeddedViewModelFactory.CreateProjectDetailsViewModel(DetailsScreen))
        );

        NavigateBack = ReactiveCommand.CreateFromObservable(() => HostScreen.Router.NavigateBack.Execute());

        OpenNewProjectForm = ReactiveCommand.CreateFromObservable(() =>
            DetailsScreen.Router.NavigateAndReset.Execute(
                _embeddedViewModelFactory.CreateNewProjectFormViewModel(DetailsScreen, ActiveProjectGroup)));

        OpenEditProjectForm = ReactiveCommand.CreateFromObservable(() =>
            DetailsScreen.Router.NavigateAndReset.Execute(
                _embeddedViewModelFactory.CreateEditProjectFormViewModel(DetailsScreen)));

        /*
        NavigateToTasksForProject = ReactiveCommand.CreateFromObservable(() => 
            HostScreen.Router.Navigate.Execute(_routableViewModelFactory.CreateTasksViewModel(HostScreen)));
        */
        NavigateToTasksForProject = ReactiveCommand.CreateFromObservable(PrepareTasksViewModel);

        OpenProject = ReactiveCommand.Create(RespondToOpenProject);

        var canDeleteSelectedProject = this.WhenAnyValue(vm => vm.ProjectsPaginatedList.ItemIsSelected);
        DeleteSelectedProject = ReactiveCommand.Create(RespondToDeleteSelectedProject, canDeleteSelectedProject);

        DeleteSelectedProject.IsExecuting
            .Do(_ => DetailsScreen.Router.NavigationStack.Clear())
            .Subscribe();

        // TODO: figure out how to use InvokeCommand operator
        this.WhenAnyValue(vm => vm.ProjectsPaginatedList.SelectedItem)
            .WhereNotNull()
            .Select(project => project.Name)
            .Do(projectName =>
            {
                OpenProjectDetails.Execute().Subscribe();
                var detailsViewModel = (ProjectDetailsViewModel)DetailsScreen.CurrentViewModel;
                detailsViewModel.Header = projectName;
            })
            .Subscribe();

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public ReactiveCommand<Unit, IRoutableViewModel> NavigateBack { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> OpenNewProjectForm { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> OpenProjectDetails { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> OpenEditProjectForm { get; }
    public ReactiveCommand<Unit, Unit> OpenProject { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToTasksForProject { get; }
    public ReactiveCommand<Unit, Unit> DeleteSelectedProject { get; }

    public EmbeddedDetailsScreenViewModel DetailsScreen { get; }
    public VaultHeaderMenuViewModel HeaderMenu { get; }
    public PaginatedContentListBoxViewModel<ProjectModel> ProjectsPaginatedList { get; }

    private IEnumerable<ProjectModel> LoadedProjects
    {
        get => _loadedProjects;
        set => this.RaiseAndSetIfChanged(ref _loadedProjects, value);
    }

    public string Header
    {
        get => _header;
        set => this.RaiseAndSetIfChanged(ref _header, value);
    }

    public ProjectGroupModel ActiveProjectGroup
    {
        get => _activeProjectGroup;
        set => this.RaiseAndSetIfChanged(ref _activeProjectGroup, value);
    }

    public string ProjectsListBoxControlName { get; } = "ProjectsListBox";

    public string NoProjectsMessage { get; } = "No projects in this project group";
    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "projects";
    public IScreen HostScreen { get; }

    private void ReloadProjects()
    {
        LoadedProjects = _applicationDataStorage.LoadProjectsInProjectGroup(ActiveProjectGroup).ToList();
        ProjectsPaginatedList.FullContent = LoadedProjects.Select(items => items);
    }

    private void RespondToOpenProject()
    {
        NavigateToTasksForProject.Execute().Subscribe();
    }

    private void RespondToDeleteSelectedProject()
    {
        _applicationDataStorage.DeleteContent(ProjectsPaginatedList.SelectedItem);
        ReloadProjects();
    }

    private IObservable<IRoutableViewModel> PrepareTasksViewModel()
    {
        var tasksViewModel =
            _routableViewModelFactory.CreateTasksViewModel(HostScreen, ProjectsPaginatedList.SelectedItem);

        return HostScreen.Router.Navigate.Execute(tasksViewModel);
    }
}