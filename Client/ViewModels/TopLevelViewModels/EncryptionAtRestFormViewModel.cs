// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;

namespace TaskVaultNative.Client.ViewModels;

public class EncryptionAtRestFormViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel,
    IValidatableViewModel
{
    private bool _encryptionAtRestDescriptionVisible;

    private ObservableAsPropertyHelper<string> _errorMessages;
    private readonly ILocalVaultBuilderService _localVaultBuilder;
    private readonly IRoutableViewModelFactory _routableViewModelFactory;

    public EncryptionAtRestFormViewModel(IScreen screen, IRoutableViewModelFactory routableViewModelFactory,
        ILocalVaultBuilderService localVaultBuilder)
    {
        Activator = new ViewModelActivator();

        HostScreen = screen;
        _routableViewModelFactory = routableViewModelFactory;
        _localVaultBuilder = localVaultBuilder;

        // TODO: Debug
        this.WhenAnyValue(vm => vm.ValidationContext.IsValid).ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(valid => Console.WriteLine($"Validation State {valid}"));
        this.WhenAnyValue(vm => vm.EncryptionAtRestDescriptionVisible).ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(visible => Console.WriteLine($"Description Visible {visible}"));
        Console.WriteLine($"Vault build name: {_localVaultBuilder.NewLocalVaultBuild?.Name}");
        ////

        NavigateBack = ReactiveCommand.Create(() => { HostScreen.Router.NavigateBack.Execute(); });

        CancelProcess = ReactiveCommand.CreateFromObservable(() => HostScreen.Router.NavigateAndReset
            .Execute(_routableViewModelFactory.CreateLocalVaultSelectViewModel(HostScreen)));

        DeclineToConfigure = ReactiveCommand.Create(RespondToDeclineToConfigure);

        NavigateToNextForm = ReactiveCommand.CreateFromObservable(() =>
            HostScreen.Router.Navigate.Execute(
                _routableViewModelFactory.CreateConnectToRemoteFormViewModel(HostScreen)));

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public string ErrorMessages => _errorMessages.Value;

    public ReactiveCommand<Unit, Unit> NavigateBack { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> CancelProcess { get; }
    public ReactiveCommand<Unit, Unit> SubmitForm { get; }
    public ReactiveCommand<Unit, Unit> DeclineToConfigure { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToNextForm { get; }

    public string? HeaderPrompt { get; } = "Configure encryption-at-rest for this local vault?";

    public string? PasswordFieldWatermark { get; } = "Enter Local Vault Password";
    public string? ConfirmPasswordFieldWatermark { get; } = "Confirm Password";

    public string? EncryptionAtRestDescription { get; } =
        "\"Encryption-at-rest\" refers to the practice of using an encryption key to encrypt all user data " +
        "before it is saved to system storage. When TaskVault needs to access the data, it is loaded from system storage and " +
        "decrypted in memory. This ensures the data cannot be accessed by unauthorized users when the app is not in use. " +
        "\n\n" +
        "Warning: the encryption key will be derived from the local vault password. If this password is lost, it will not be possible " +
        "to recover the encrypted data. Please ensure the password is not lost by using a secure mechanism such as a password management app. ";

    public bool EncryptionAtRestDescriptionVisible
    {
        get => _encryptionAtRestDescriptionVisible;
        set => this.RaiseAndSetIfChanged(ref _encryptionAtRestDescriptionVisible, value);
    }

    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "encryption-at-rest-form";

    public IScreen HostScreen { get; }

    public ValidationContext ValidationContext { get; } = new();

    private void RespondToDeclineToConfigure()
    {
        _localVaultBuilder.SetEncryptionAtRestToDisabled();
        NavigateToNextForm.Execute();
    }
}