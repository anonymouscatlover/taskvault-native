// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using TaskVaultNative.Client.Exceptions;

namespace TaskVaultNative.Client.ViewModels;

public class PaginatedContentListBoxViewModel<TContent> : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private readonly ObservableAsPropertyHelper<IEnumerable<TContent>> _currentPageContent;

    private IEnumerable<TContent> _fullContent = new List<TContent>();

    private readonly ObservableAsPropertyHelper<bool> _isEmpty;

    private readonly ObservableAsPropertyHelper<bool> _itemIsSelected;

    private readonly int _itemsPerPage = 10;

    //TODO: add to unit tests
    private double _listBoxHeight;

    private TContent _selectedItem;

    private int _sourceIndexOfFirstItemOnPage;

    public PaginatedContentListBoxViewModel(IScreen screen)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _currentPageContent =
            this.WhenAnyValue(vm => vm.FullContent,
                    vm => vm.SourceIndexOfItemOnPage,
                    (content, index) => { return content.ToList().Skip(index).Take(_itemsPerPage); })
                .ToProperty(this, vm => vm.CurrentPageContent);

        _isEmpty = this.WhenAnyValue(vm => vm.FullContent)
            .Select(items => !items.Any())
            .ToProperty(this, vm => vm.IsEmpty);

        _itemIsSelected = this.WhenAnyValue(vm => vm.SelectedItem)
            .Select(item => item != null)
            .ToProperty(this, vm => vm.ItemIsSelected);

        var canShiftToNextPage = this.WhenAnyValue(vm => vm.SourceIndexOfItemOnPage,
            vm => vm.FullContent,
            (index, fullContent) => index + _itemsPerPage < fullContent.Count());
        ShiftToNextPage = ReactiveCommand.Create(() => RespondToShiftToNextPage(), canShiftToNextPage);

        var canShiftToPreviousPage = this.WhenAnyValue(vm => vm.SourceIndexOfItemOnPage,
            vm => vm.FullContent,
            (index, fullContent) => index - _itemsPerPage >= 0);
        ShiftToPreviousPage = ReactiveCommand.Create(() => RespondToShiftToPreviousPage(), canShiftToPreviousPage);

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public string Name { get; set; } = "ContentListBox";
    public string NoContentMessage { get; set; } = "No content";
    public string NoContentMessageControlName { get; set; } = "NoContentMessage";
    public IEnumerable<TContent> CurrentPageContent => _currentPageContent.Value;

    public IEnumerable<TContent> FullContent
    {
        get => _fullContent;
        set => this.RaiseAndSetIfChanged(ref _fullContent, value);
    }

    private int SourceIndexOfItemOnPage
    {
        get => _sourceIndexOfFirstItemOnPage;
        set => this.RaiseAndSetIfChanged(ref _sourceIndexOfFirstItemOnPage, value);
    }

    public bool IsEmpty => _isEmpty.Value;

    public TContent SelectedItem
    {
        get => _selectedItem;
        set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
    }

    public bool ItemIsSelected => _itemIsSelected.Value;

    public ReactiveCommand<Unit, Unit> ShiftToNextPage { get; }
    public ReactiveCommand<Unit, Unit> ShiftToPreviousPage { get; }

    public double ListBoxHeight
    {
        get => _listBoxHeight;
        set => this.RaiseAndSetIfChanged(ref _listBoxHeight, value);
    }

    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "paginated-content-list-box";
    public IScreen HostScreen { get; }

    private void RespondToShiftToNextPage()
    {
        ShiftFirstItemIndexToTopOfNextPage();
        UpdateCurrentPageContentBasedOnSourceIndex();
    }

    private void ShiftFirstItemIndexToTopOfNextPage()
    {
        if (SourceIndexOfItemOnPage + _itemsPerPage >= FullContent.Count())
            throw new InvalidPageRequestException("Attempted to shift to next page from final page");
        SourceIndexOfItemOnPage += _itemsPerPage;
    }

    private void RespondToShiftToPreviousPage()
    {
        ShiftFirstItemIndexToTopOfPreviousPage();
        UpdateCurrentPageContentBasedOnSourceIndex();
    }

    private void ShiftFirstItemIndexToTopOfPreviousPage()
    {
        if (SourceIndexOfItemOnPage - _itemsPerPage < 0)
            throw new InvalidPageRequestException("Attempted to shift to previous page from first page");
        SourceIndexOfItemOnPage -= _itemsPerPage;
    }

    private void UpdateCurrentPageContentBasedOnSourceIndex()
    {
        var numberOfItemsOnPage = GetNumberOfItemsToDisplayOnCurrentPage();
        var currentRange = FullContent.ToList().GetRange(SourceIndexOfItemOnPage, numberOfItemsOnPage);

        //CurrentPageContent.Clear();
        //CurrentPageContent.AddRange(currentRange);
    }

    private int GetNumberOfItemsToDisplayOnCurrentPage()
    {
        var numberOfRemainingItems = GetNumberOfItemsFollowingCurrentPage();
        var numberOfItemsOnPage = Math.Min(_itemsPerPage, numberOfRemainingItems);

        return numberOfItemsOnPage;
    }

    private int GetNumberOfItemsFollowingCurrentPage()
    {
        var totalNumberOfItemsPresentedSoFar = SourceIndexOfItemOnPage;
        var numberOfRemainingItems = FullContent.Count() - totalNumberOfItemsPresentedSoFar;
        if (numberOfRemainingItems < 0) numberOfRemainingItems = 0;

        return numberOfRemainingItems;
    }
}