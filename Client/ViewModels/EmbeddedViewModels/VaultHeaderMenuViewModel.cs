// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Reactive;
using System.Reactive.Disposables;
using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;

namespace TaskVaultNative.Client.ViewModels;

public class VaultHeaderMenuViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private LocalVaultModel _activeLocalVault = new();

    private readonly IRoutableViewModelFactory _routableViewModelFactory;

    public VaultHeaderMenuViewModel(IScreen screen, IRoutableViewModelFactory routableViewModelFactory,
        LocalVaultModel activeLocalVault)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _routableViewModelFactory = routableViewModelFactory;
        ActiveLocalVault = activeLocalVault ?? new LocalVaultModel();

        CloseVault = ReactiveCommand.Create(RespondToCloseVault);

        ReturnToLocalVaultSelect = ReactiveCommand.CreateFromObservable(() =>
            HostScreen.Router.NavigateAndReset.Execute(
                _routableViewModelFactory.CreateLocalVaultSelectViewModel(HostScreen)));

        NavigateToMainMenu = ReactiveCommand.CreateFromObservable(() =>
            HostScreen.Router.NavigateAndReset.Execute(_routableViewModelFactory
                .CreateMainMenuViewModel(HostScreen, ActiveLocalVault)));

        NavigateToInbox = ReactiveCommand.CreateFromObservable(() =>
            HostScreen.Router.NavigateAndReset.Execute(_routableViewModelFactory
                .CreateInboxViewModel(HostScreen, ActiveLocalVault)));
        
        NavigateToAbout = ReactiveCommand.CreateFromObservable(() =>
            HostScreen.Router.Navigate.Execute(_routableViewModelFactory.CreateAboutViewModel(HostScreen)));

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public ReactiveCommand<Unit, Unit> CloseVault { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> ReturnToLocalVaultSelect { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToMainMenu { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToInbox { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToAbout { get; }

    public LocalVaultModel ActiveLocalVault
    {
        get => _activeLocalVault;
        private set => this.RaiseAndSetIfChanged(ref _activeLocalVault, value);
    }

    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "view-model-url";
    public IScreen HostScreen { get; }

    public void RespondToCloseVault()
    {
        ReturnToLocalVaultSelect.Execute();
    }
}