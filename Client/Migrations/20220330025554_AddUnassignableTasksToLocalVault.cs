﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskVaultNative.Client.Migrations
{
    public partial class AddUnassignableTasksToLocalVault : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LocalVaultId",
                table: "TaskModels",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaskModels_LocalVaultId",
                table: "TaskModels",
                column: "LocalVaultId");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskModels_LocalVaultModels_LocalVaultId",
                table: "TaskModels",
                column: "LocalVaultId",
                principalTable: "LocalVaultModels",
                principalColumn: "LocalVaultId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskModels_LocalVaultModels_LocalVaultId",
                table: "TaskModels");

            migrationBuilder.DropIndex(
                name: "IX_TaskModels_LocalVaultId",
                table: "TaskModels");

            migrationBuilder.DropColumn(
                name: "LocalVaultId",
                table: "TaskModels");
        }
    }
}
