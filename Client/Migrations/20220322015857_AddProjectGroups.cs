﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskVaultNative.Client.Migrations
{
    public partial class AddProjectGroups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProjectGroupModels",
                columns: table => new
                {
                    ProjectGroupId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SyncId = table.Column<Guid>(type: "TEXT", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    LocalVaultId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectGroupModels", x => x.ProjectGroupId);
                    table.ForeignKey(
                        name: "FK_ProjectGroupModels_LocalVaultModels_LocalVaultId",
                        column: x => x.LocalVaultId,
                        principalTable: "LocalVaultModels",
                        principalColumn: "LocalVaultId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectGroupModels_LocalVaultId",
                table: "ProjectGroupModels",
                column: "LocalVaultId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectGroupModels_SyncId",
                table: "ProjectGroupModels",
                column: "SyncId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectGroupModels");
        }
    }
}
