// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Splat;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.ViewModels;
using TaskVaultNative.Client.Views;

namespace TaskVaultNative.Client;

public class App : Application
{
    private IConfiguration? _appConfiguration;
    private ILogger? _logger;
    private IServiceProvider _serviceProvider;

    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    // The entrypoint for your application. Here you initialize your
    // MVVM framework, DI container and other components. You can use
    // the ApplicationLifetime property here to detect if the app
    // is running on a desktop platform or on a mobile platform.
    public override void OnFrameworkInitializationCompleted()
    {
        ConfigureConfigurationProvider();
        ConfigureServiceProvider();
        ConfigureLogging();
        ConfigureApplicationDatabase();
        ConfigureMainWindowBasedOnPlatform();

        base.OnFrameworkInitializationCompleted();
    }

    private void ConfigureConfigurationProvider()
    {
        string configFilename;
#if DEBUG
        if (Environment.GetEnvironmentVariable("Test") == "true")
        {
            configFilename = "appsettings.test.json";
        }
        else
        {
            configFilename = "appsettings.development.json";
        }
#else
        if (Environment.GetEnvironmentVariable("Test") == "true")
        {
            configFilename = "appsettings.test.json";
        }
        else
        {
            configFilename = "appsettings.json";
        }
#endif
        _appConfiguration = new AppConfigurationLoader(configFilename).AppConfiguration;
    }

    private void ConfigureLogging()
    {
    }

    private void ConfigureApplicationDatabase()
    {
        using (var serviceScope = _serviceProvider.CreateScope())
        {
            var context = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();
        }
    }

    private void ConfigureServiceProvider()
    {
        _serviceProvider = new ServiceContainer(_appConfiguration);
    }

    private void ConfigureMainWindowBasedOnPlatform()
    {
        if (RunningOnDesktopPlatform()) ConfigureMainWindowForDesktopPlatform();
    }

    private bool RunningOnDesktopPlatform()
    {
        return ApplicationLifetime is IClassicDesktopStyleApplicationLifetime;
    }

    private void ConfigureMainWindowForDesktopPlatform()
    {
        var mainWindowViewModel = _serviceProvider.GetService<MainWindowViewModel>();
        var desktop = (IClassicDesktopStyleApplicationLifetime)ApplicationLifetime;

        desktop.MainWindow = new MainWindow { DataContext = mainWindowViewModel };
    }
}