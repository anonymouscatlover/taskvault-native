// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.ViewModels;

namespace TaskVaultNative.Client.Services;

public interface IEmbeddedViewModelFactory
{
    public EmbeddedDetailsScreenViewModel CreateEmbeddedDetailsScreenViewModel();

    public VaultHeaderMenuViewModel CreateVaultHeaderMenuViewModel(IScreen screen,
        LocalVaultModel activeLocalVault = null);

    public NewProjectGroupFormViewModel CreateNewProjectGroupFormViewModel(IScreen screen,
        LocalVaultModel activeLocalVault = null);

    public PaginatedContentListBoxViewModel<TContent> CreatePaginatedContentListBoxViewModel<TContent>(IScreen screen);
    public ProjectGroupDetailsViewModel CreateProjectGroupDetailsViewModel(IScreen screen);
    public EditProjectGroupFormViewModel CreateEditProjectGroupFormViewModel(IScreen screen);

    public NewProjectFormViewModel CreateNewProjectFormViewModel(IScreen screen,
        ProjectGroupModel activeProjectGroup = null);

    public ProjectDetailsViewModel CreateProjectDetailsViewModel(IScreen screen);
    public EditProjectFormViewModel CreateEditProjectFormViewModel(IScreen screen);

    public NewTaskFormViewModel CreateNewTaskFormViewModel(IScreen screen, LocalVaultModel activeLocalVault = null,
        ProjectModel activeProject = null);

    public TaskDetailsViewModel CreateTaskDetailsViewModel(IScreen screen);
    public EditTaskFormViewModel CreateEditTaskFormViewModel(IScreen screen);
    public MoveTaskViewModel CreateMoveTaskViewModel(IScreen screen, LocalVaultModel activeLocalVault = null);
}